# Cocor

stands for *compiler compiler* generating *recursive* descent parsers, which takes an attributed grammar of a source language and generates a scanner and a parser for this language. The scanner works as a deterministic finite automaton. The parser uses recursive descent. LL(1) conflicts can be resolved by a multi-symbol lookahead or by semantic checks. Thus the class of accepted grammars is LL(k) for an arbitrary k. 

**This is a C++ fork of Coco/R (http://www.ssw.uni-linz.ac.at/Coco/)** with the aim to have it on a modern toolchain producing ready-to-use crosscompiled binaries as downloadable artifacts for other projects. 

 - Original sources: https://ssw.jku.at/Research/Projects/Coco/CPP/CocoSourcesCPP.zip

Current version 2.0.1:

 - Build Windows 64 bit executable with GCC, cross compiling using MingGW
 - Build Linux 64 bit executable with Clang and GCC
 - Build html documentation with doxygen
 - Builds are deterministic (verified)
 - Automatic deployment to GitLab pages
 - Automatic release-info injection
 
### Deployed Sourcecode Documentation 

 - Doxygen: https://cascade-gmbh.gitlab.io/cocor/doxygen/

### Deployed Executables

  - Linux 64 bit (gcc): https://cascade-gmbh.gitlab.io/cocor/bin/lin64gcc/cocor
  - Linux 64 bit (clang): https://cascade-gmbh.gitlab.io/cocor/bin/lin64clang/cocor
  - Windows 64 bit (gcc): https://cascade-gmbh.gitlab.io/cocor/bin/win64gcc/cocor.exe

### Original Coco/R Documentation

- [UserManual.pdf](http://www.ssw.uni-linz.ac.at/Coco/Doc/UserManual.pdf) - Language specification and user guide
- [DataStructures.pdf](http://www.ssw.uni-linz.ac.at/Coco/Doc/DataStructures.pdf) - Description of the data structures in Coco/R (for those who want to understand the implementation of Coco/R)
- [JMLC'03 paper LL(1)](http://www.ssw.uni-linz.ac.at/Coco/Doc/ConflictResolvers.pdf) - Conflict Resolution in a Recursive Descent Compiler Generator
- [Tutorial](http://www.ssw.uni-linz.ac.at/Coco/Tutorial/) - Powerpoint slides of a Coco/R tutorial given at JMLC'06 (by Hanspeter Mössenböck)
- [Another Tutorial](http://structured-parsing.wikidot.com/coco-r-parser) - About using Coco/R (by Henrik Teinelund)
- [AST.pdf](http://www.ssw.uni-linz.ac.at/Coco/Doc/AST.pdf) - Tutorial showing how to build abstract syntax trees with Coco/R
- [Empty.atg](http://www.ssw.uni-linz.ac.at/Coco/Contributions/Any/Empty.atg) - A documented, empty template for atg files.
