cmake_minimum_required(VERSION 3.1)

project(coror)
  
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Link project warnings 'library' to use the following warnings
add_library(project_warnings INTERFACE)
if(MSVC)
  target_compile_options(project_warnings INTERFACE /W4)
else()
  target_compile_options(project_warnings INTERFACE
    -Wall
    -Wextra            # reasonable and standard
    -Wpedantic         # warn if non-standard C++ is used
    -Wshadow           # warn the user if a variable declaration shadows 
                       # one from a parent context  
    -Wnon-virtual-dtor # warn the user if a class with virtual functions has
                       # a non-virtual destructor. This helps catch hard to
                       # track down memory errors
    -Wold-style-cast   # warn for c-style casts
    -Wcast-align       # warn for potential performance problem casts
    -Wunused           # warn on anything being unused
    -Wconversion       # warn on type conversions that may lose data
    -Wsign-conversion  # warn on sign conversions
    -Wlogical-op       # warn about logical operations being used where
                       # bitwise were probably wanted
    -Wnull-dereference # warn if a null dereference is detected
    -Wuseless-cast     # warn if you perform a cast to the same type
    -Wdouble-promotion # warn if float is implicit promoted to double
    -Wformat=2         # warn on security issues around functions that
                       # format output (ie printf)
    -Wduplicated-cond        # warn if if / else chain 
                             # has duplicated conditions
    -Wduplicated-branches    # warn if if / else branches 
                             # have duplicated code
    -Woverloaded-virtual     # warn if you overload (not override) 
                             # a virtual function 
    -Wmisleading-indentation # warn if identation implies blocks 
                             # where blocks do not exist
    )
endif() 

# read release-info:
include(../release-info.cmake)

# cmake variable substitution:
configure_file( Cocor.cpp ${CMAKE_CURRENT_LIST_DIR}/Cocor.cxx)
        
add_executable( cocor Cocor.cxx     
                      ../forked/Action.cpp 
                      ../forked/ArrayList.cpp 
                      ../forked/BitArray.cpp 
                      ../forked/CharClass.cpp 
                      ../forked/CharSet.cpp 
                      ../forked/Comment.cpp 
                      ../forked/DFA.cpp 
                      ../forked/Generator.cpp 
                      ../forked/HashTable.cpp 
                      ../forked/Melted.cpp 
                      ../forked/Node.cpp 
                      ../forked/Parser.cpp 
                      ../forked/ParserGen.cpp 
                      ../forked/Position.cpp 
                      ../forked/Scanner.cpp 
                      ../forked/SortedList.cpp 
                      ../forked/State.cpp 
                      ../forked/StringBuilder.cpp
                      ../forked/Symbol.cpp 
                      ../forked/Tab.cpp 
                      ../forked/Target.cpp                      
                      ../forked/Action.h 
                      ../forked/ArrayList.h 
                      ../forked/BitArray.h 
                      ../forked/CharClass.h 
                      ../forked/CharSet.h 
                      ../forked/Comment.h 
                      ../forked/DFA.h 
                      ../forked/Generator.h 
                      ../forked/HashTable.h 
                      ../forked/Melted.h 
                      ../forked/Node.h 
                      ../forked/Parser.h 
                      ../forked/ParserGen.h 
                      ../forked/Position.h 
                      ../forked/Scanner.h 
                      ../forked/SortedList.h 
                      ../forked/State.h 
                      ../forked/StringBuilder.h
                      ../forked/Symbol.h 
                      ../forked/Tab.h 
                      ../forked/Target.h )  
   
 


